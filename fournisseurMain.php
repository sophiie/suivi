<!DOCTYPE html>
<html lang="fr">

  <head>

    <meta charset="utf-16">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">

    <title>Gestion Fournisseur</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="assets/css/fontawesome.css">
    <link rel="stylesheet" href="assets/css/templatemo-style.css">
    <link rel="stylesheet" href="assets/css/owl.css">

  </head>

<body class="is-preload">
    <!-- Wrapper -->
    <div id="wrapper">

      <!-- Main -->
        <div id="main">
          <div class="inner">


			<?php include("sidebar.php");
				if (isset($_GET['message']) && $_GET['message'] == '1'){
					echo "<div>Fournisseur ajouté avec succès</div>";
				}
				if (isset($_GET['message']) && $_GET['message'] == '2'){
					echo "<div>Fournisseur supprimé avec succès</div>";
				}
			?>

<br>
      <div class="col-md-6">
						<div class="border-rounded-button"><a href="fournisseurAdd.php">Ajout Fournisseur</a></div>
      </div>
 <div class="alternate-table">
  <table>
    <thead>
      <tr>
        <th>Fournisseur</th>
        <th>Addresse</th>
        <th>Tél</th>
        <th width="25%">Actions</th>
      </tr>
    </thead>
    <tbody>
		<?php include("config/connect.php"); 

	$sql = "SELECT * FROM forniseur";
	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
	  // output data of each row
	  while($row = $result->fetch_assoc()) {	
      echo "<tr>	 
					<td>".$row['designation']."</td>
					<td>".$row['addfornisseur']."</td>
					<td>".$row['telfornisseur']."</td>
					<td>	
              <a href='fournisseurConsulter.php?idfornisseur=".$row['idfornisseur']."'>Consulter</a>
              <a href='fournisseurEditer.php?idfornisseur=".$row['idfornisseur']."'>Editer</a>
              <a href='fournisseurSupprimer.php?idfornisseur=".$row['idfornisseur']."'>Supprimer</a>
					</td>  
				</tr>";
	  }
	} else {
	  echo "Pas des fournisseurs";
	}


?>				
						  
                        </tbody>
                      </table>
                      </ul>
                    </div>
          </div>
	    </div>
	</div>

  <!-- Scripts -->
  <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/browser.min.js"></script>
    <script src="assets/js/breakpoints.min.js"></script>
    <script src="assets/js/transition.js"></script>
    <script src="assets/js/owl-carousel.js"></script>
    <script src="assets/js/custom.js"></script>
</body>


  </body>
</html>