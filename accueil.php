<!DOCTYPE html>
<html lang="fr">

  <head>

    <meta charset="utf-16">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">

    <title>Ramayana - Free Bootstrap 4 CSS Template</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="assets/css/fontawesome.css">
    <link rel="stylesheet" href="assets/css/templatemo-style.css">
    <link rel="stylesheet" href="assets/css/owl.css">

  </head>

<body class="is-preload">
    <!-- Wrapper -->
    <div id="wrapper">
      <!-- Main -->
        <div id="main">
          <div class="inner">


			<?php include("sidebar.php"); ?>
      <div align="center"  style="font-size:3vw">Statistiques de gestion</div>
       <div class="alternate-table">
          <table>
							<tr>
              <td width='50%'>Nombre établimement</td>
                <td>
                  <?php include("config/connect.php");     
                  $sql = "SELECT count(*) FROM etablissement";
                  $result = $conn->query($sql);
                  $row = $result->fetch_assoc();
                  echo $row['count(*)'];
                  ?>
                </td>
              </tr>     
              <tr>
              <td width='50%'>Nombre Equipement</td>
                <td>
                  <?php include("config/connect.php");     
                  $sql = "SELECT count(*) FROM equipment";
                  $result = $conn->query($sql);
                  $row = $result->fetch_assoc();
                  echo $row['count(*)'];
                  ?>
                </td>
              </tr>   
              <tr>
              <td width='50%'>Nombre Fournisseur</td>
                <td>
                  <?php include("config/connect.php");     
                  $sql = "SELECT count(*) FROM forniseur";
                  $result = $conn->query($sql);
                  $row = $result->fetch_assoc();
                  echo $row['count(*)'];
                  ?>
                </td>
              </tr> 
              <tr>
              <td width='50%'>Nombre Lignes</td>
                <td>
                  <?php include("config/connect.php");     
                  $sql = "SELECT count(*) FROM lignes";
                  $result = $conn->query($sql);
                  $row = $result->fetch_assoc();
                  echo $row['count(*)'];
                  ?>
                </td>
              </tr> 
              <tr>
              <td width='50%'>Nombre Factures</td>
                <td>
                  <?php include("config/connect.php");     
                  $sql = "SELECT count(*) FROM facture";
                  $result = $conn->query($sql);
                  $row = $result->fetch_assoc();
                  echo $row['count(*)'];
                  ?>
                </td>
              </tr> 
          </table>
         </div>
        </div> 
       </div>
	    </div>

  <!-- Scripts -->
  <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/browser.min.js"></script>
    <script src="assets/js/breakpoints.min.js"></script>
    <script src="assets/js/transition.js"></script>
    <script src="assets/js/owl-carousel.js"></script>
    <script src="assets/js/custom.js"></script>
</body>


  </body>
</html>