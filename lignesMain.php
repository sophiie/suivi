<!DOCTYPE html>
<html lang="fr">

  <head>

    <meta charset="utf-16">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">

    <title>Gestion de Lignes</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="assets/css/fontawesome.css">
    <link rel="stylesheet" href="assets/css/templatemo-style.css">
    <link rel="stylesheet" href="assets/css/owl.css">

  </head>

<body class="is-preload">
    <!-- Wrapper -->
    <div id="wrapper">

      <!-- Main -->
        <div id="main">
          <div class="inner">


			<?php include("sidebar.php");
				if (isset($_GET['message']) && $_GET['message'] == '1'){
					echo "<div>Ligne ajoutée avec succès</div>";
				}
				if (isset($_GET['message']) && $_GET['message'] == '2'){
					echo "<div>Ligne supprimée avec succès</div>";
				}
			?>

<br>
                      <div class="col-md-6">
						<div class="border-rounded-button"><a href="lignesAdd.php">Ajout Ligne</a></div>
      </div>
 <div class="alternate-table">
  <table>
    <thead>
      <tr>
        <th>Etablissement</th>
        <th>Fournisseur</th>
        <th>Addresse</th>
        <th>Débit Compte</th>
        <th>Téchnologie</th>
        <th>Etat</th>
        <th width="25%">Actions</th>
      </tr>
    </thead>
    <tbody>
		<?php include("config/connect.php"); 

	$sql = "SELECT * FROM lignes";
	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
	  // output data of each row
	  while($row = $result->fetch_assoc()) {
      # Requiperer nom de l'établissments
			$sqlEtab = "SELECT * FROM etablissement where idetab=".$row['idetab'];
			$resultEtab = $conn->query($sqlEtab);
			$rowEtab = $resultEtab->fetch_assoc();

      $sqlFourn = "SELECT * FROM forniseur where 	idfornisseur=".$row['idfornisseur'];
			$resultFourn = $conn->query($sqlFourn);
			$rowFourn = $resultFourn->fetch_assoc();

      echo "<tr>	 
      <td>".$rowEtab['nom']."</td>
      <td>".$rowFourn['designation']."</td>
      <td>".$row['adress']."</td>
      <td>".$row['debitligne']."</td>
      <td>".$row['technologie']."</td>
      <td>".$row['etat']."</td>
					<td>	
              <a href='factureMain.php?idligne=".$row['idligne']."'>Gérer facture</a>
              <a href='lignesConsulter.php?idligne=".$row['idligne']."'>Consulter</a>
              <a href='lignesEditer.php?idligne=".$row['idligne']."'>Editer</a>
              <a href='lignesSupprimer.php?idligne=".$row['idligne']."'>Supprimer</a>
					</td>  
				</tr>";
	  }
	} else {
	  echo "Pas des Lignes";
	}


?>				
						  
                        </tbody>
                      </table>
                      </ul>
                    </div>
          </div>
	    </div>
	</div>

  <!-- Scripts -->
  <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/browser.min.js"></script>
    <script src="assets/js/breakpoints.min.js"></script>
    <script src="assets/js/transition.js"></script>
    <script src="assets/js/owl-carousel.js"></script>
    <script src="assets/js/custom.js"></script>
</body>


  </body>
</html>