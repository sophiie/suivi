<!DOCTYPE html>
<html lang="fr">

  <head>

    <meta charset="utf-16">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">

    <title>Gestion de Lignes</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="assets/css/fontawesome.css">
    <link rel="stylesheet" href="assets/css/templatemo-style.css">
    <link rel="stylesheet" href="assets/css/owl.css">

  </head>

<body class="is-preload">
    <!-- Wrapper -->
    <div id="wrapper">

      <!-- Main -->
        <div id="main">
          <div class="inner">


			<?php include("sidebar.php");
				if (isset($_GET['message']) && $_GET['message'] == '1'){
					echo "<script>alert('Facture ajoutée avec succès')</script>";
				}
				if (isset($_GET['message']) && $_GET['message'] == '2'){
					echo "<script>alert('Facture supprimée avec succès')</script>";
				}
        if (isset($_GET['message']) && $_GET['message'] == '3'){
					echo "<script>alert('Facture modifiée avec succès')</script>";
				}

        $idligne=$_GET['idligne']	;
			?>

<br>
      <div class="col-md-6">
						<div class="border-rounded-button"><a href="factureAdd.php?idligne=<?php echo $idligne;?>">Ajout Facture</a></div>
      </div>
 <div class="alternate-table">
  <table>
    <thead>
      <tr>
        <th>Etablisement</th>
        <th>Addresse</th>
        <th>Consommation</th>
        <th>Date Facturation</th>
        <th width="25%">Actions</th>
      </tr>
    </thead>
    <tbody>
		<?php include("config/connect.php"); 

    
    $sql = "SELECT * FROM facture where idcompte=$idligne";
    $result = $conn->query($sql);

	if ($result->num_rows > 0) {
	  // output data of each row
	  while($row = $result->fetch_assoc()) {
      # Requiperer nom de l'établissments
			$sqlEtab = "SELECT * FROM etablissement where idetab=(select idetab from lignes where idligne = ".$idligne.")";
			$resultEtab = $conn->query($sqlEtab);
			$rowEtab = $resultEtab->fetch_assoc();

      $sqlLigne = "SELECT * FROM lignes where 	idligne=".$idligne;
			$resultLigne = $conn->query($sqlLigne);
			$rowLigne = $resultLigne->fetch_assoc();

      echo "<tr>	 
      <td>".$rowEtab['nom']."</td>
      <td>".$rowLigne['adress']."</td>
      <td>".$row['consommation']."</td>
      <td>".$row['dateFacturation']."</td>
  
					<td>	
              <a href='factureConsulter.php?idfacture=".$row['idfacture']."&idligne=".$idligne."'>Consulter</a>
              <a href='factureEditer.php?idfacture=".$row['idfacture']."&idligne=".$idligne."'>Editer</a>
              <a href='factureSupprimer.php?idfacture=".$row['idfacture']."&idligne=".$idligne."'>Supprimer</a>
					</td>  
				</tr>";
	  }
	} else {
	  echo "Pas des Factures";
	}


?>				
						  
              </tbody>
            </table>
            </ul>
          </div>
        </div>
    </div>
	</div>

  <!-- Scripts -->
  <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/browser.min.js"></script>
    <script src="assets/js/breakpoints.min.js"></script>
    <script src="assets/js/transition.js"></script>
    <script src="assets/js/owl-carousel.js"></script>
    <script src="assets/js/custom.js"></script>
</body>


  </body>
</html>