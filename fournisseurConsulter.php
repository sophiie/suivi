<!DOCTYPE html>
<html lang="fr">

  <head>

    <meta charset="utf-16">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">

    <title>Ramayana - Free Bootstrap 4 CSS Template</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="assets/css/fontawesome.css">
    <link rel="stylesheet" href="assets/css/templatemo-style.css">
    <link rel="stylesheet" href="assets/css/owl.css">

  </head>

<body class="is-preload">
    <!-- Wrapper -->
    <div id="wrapper">

      <!-- Main -->
        <div id="main">
          <div class="inner">


			<?php include("sidebar.php"); ?>



	<?php include("config/connect.php"); 

    $idfornisseur=$_GET['idfornisseur'];

	$sql = "SELECT * FROM forniseur where idfornisseur = $idfornisseur";
	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
	  // output data of each row
	  if($row = $result->fetch_assoc()) {
		  ?>



 <div class="alternate-table">
                      <table>

                        <tbody>
							<tr>
								<td width="50%">Fournisseur</td>
								<td><?php echo $row['designation'];?></td>
							</tr>
							<tr>
								<td>Addresse</td>
								<td><?php echo $row['addfornisseur'];?></td>
							</tr>
							<tr>
								<td>Tél</td>
								<td><?php echo $row['telfornisseur'];?></td>
							</tr>
                        </tbody>
                      </table>
                    </div>

               <table><tr>
               <td width="50%"><div class="border-rounded-button">
                    <a href="fournisseurMain.php">Retour au gestion des fournisseurs</a>
                     </div>
                     </td>
               <td><div class="border-rounded-button">
               <a href="fournisseurEditer.php?idfornisseur=<?php echo $idfornisseur;?>">Modifier les informations</a>
                     </div></td>
               </tr> 
               </table>

            </div>                  


<?php  }
	} else {
	  echo "Pas des fournisseur";
	}
	?>










   
          </div>
	    </div>
	</div>

  <!-- Scripts -->
  <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/browser.min.js"></script>
    <script src="assets/js/breakpoints.min.js"></script>
    <script src="assets/js/transition.js"></script>
    <script src="assets/js/owl-carousel.js"></script>
    <script src="assets/js/custom.js"></script>
</body>


  </body>
</html>