-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mer 30 Juin 2021 à 19:45
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `suivi`
--
CREATE DATABASE IF NOT EXISTS `suivi` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `suivi`;

-- --------------------------------------------------------

--
-- Structure de la table `equipment`
--

DROP TABLE IF EXISTS `equipment`;
CREATE TABLE IF NOT EXISTS `equipment` (
  `idequip` int(11) NOT NULL AUTO_INCREMENT,
  `idetab` int(11) DEFAULT NULL,
  `DesignationEquip` varchar(254) DEFAULT NULL,
  `model` varchar(254) DEFAULT NULL,
  `etatCreation` varchar(254) DEFAULT NULL,
  PRIMARY KEY (`idequip`),
  KEY `AK_idEquipementPK` (`idequip`),
  KEY `FK_Association_2` (`idetab`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Contenu de la table `equipment`
--

INSERT INTO `equipment` (`idequip`, `idetab`, `DesignationEquip`, `model`, `etatCreation`) VALUES
(10, 2, 'Routeur Cisco 3840', 'Cisco 3840', 'Actif');

-- --------------------------------------------------------

--
-- Structure de la table `etablissement`
--

DROP TABLE IF EXISTS `etablissement`;
CREATE TABLE IF NOT EXISTS `etablissement` (
  `idetab` int(11) NOT NULL AUTO_INCREMENT,
  `idUser` int(11) NOT NULL,
  `fo` varchar(254) DEFAULT NULL,
  `nom` varchar(254) DEFAULT NULL,
  `debit` int(11) DEFAULT NULL,
  `debupgrade` int(11) DEFAULT NULL,
  `dateupgrade` date DEFAULT NULL,
  `datecreation` date DEFAULT NULL,
  `iplan` varchar(254) DEFAULT NULL,
  `ipwan` varchar(254) DEFAULT NULL,
  `adresse` varchar(254) DEFAULT NULL,
  `tel` varchar(254) DEFAULT NULL,
  `responsable` varchar(254) DEFAULT NULL,
  PRIMARY KEY (`idetab`),
  KEY `FK_Association_3` (`idUser`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `etablissement`
--

INSERT INTO `etablissement` (`idetab`, `idUser`, `fo`, `nom`, `debit`, `debupgrade`, `dateupgrade`, `datecreation`, `iplan`, `ipwan`, `adresse`, `tel`, `responsable`) VALUES
(2, 1, '1', 'Iset Siliana', 100, 50, '2018-06-20', '2003-04-12', '41.223.225.21', '41.223.225.22', 'Siliana', '78234556', 'Mohamed Dhaoui');

-- --------------------------------------------------------

--
-- Structure de la table `facture`
--

DROP TABLE IF EXISTS `facture`;
CREATE TABLE IF NOT EXISTS `facture` (
  `idfacture` int(11) NOT NULL AUTO_INCREMENT,
  `idcompte` int(11) NOT NULL,
  `consommation` varchar(254) DEFAULT NULL,
  `dateFacturation` date DEFAULT NULL,
  PRIMARY KEY (`idfacture`),
  KEY `AK_Identifier_2` (`idfacture`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Contenu de la table `facture`
--

INSERT INTO `facture` (`idfacture`, `idcompte`, `consommation`, `dateFacturation`) VALUES
(21, 12, '360', '2012-11-26');

-- --------------------------------------------------------

--
-- Structure de la table `forniseur`
--

DROP TABLE IF EXISTS `forniseur`;
CREATE TABLE IF NOT EXISTS `forniseur` (
  `idfornisseur` int(11) NOT NULL AUTO_INCREMENT,
  `addfornisseur` varchar(254) DEFAULT NULL,
  `telfornisseur` int(11) DEFAULT NULL,
  `designation` varchar(254) DEFAULT NULL,
  PRIMARY KEY (`idfornisseur`),
  KEY `AK_idfornisseur` (`idfornisseur`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `forniseur`
--

INSERT INTO `forniseur` (`idfornisseur`, `addfornisseur`, `telfornisseur`, `designation`) VALUES
(1, 'Le Bardo1', 71223554, 'Ooredoo'),
(2, 'LA Kasba', 71234561, 'Tunisie TÃ©lecom');

-- --------------------------------------------------------

--
-- Structure de la table `lignes`
--

DROP TABLE IF EXISTS `lignes`;
CREATE TABLE IF NOT EXISTS `lignes` (
  `idligne` int(11) NOT NULL AUTO_INCREMENT,
  `idetab` int(11) NOT NULL,
  `idfornisseur` int(11) NOT NULL,
  `adress` varchar(254) DEFAULT NULL,
  `debitligne` int(11) DEFAULT NULL,
  `datecloture` date DEFAULT NULL,
  `technologie` varchar(254) DEFAULT NULL,
  `etat` varchar(20) DEFAULT NULL,
  `datemission` date DEFAULT NULL,
  PRIMARY KEY (`idligne`),
  KEY `FK_Association_1` (`idetab`),
  KEY `FK_Association_6` (`idfornisseur`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Contenu de la table `lignes`
--

INSERT INTO `lignes` (`idligne`, `idetab`, `idfornisseur`, `adress`, `debitligne`, `datecloture`, `technologie`, `etat`, `datemission`) VALUES
(12, 2, 2, '41.223.145.201', 30, '0000-00-00', 'Ligne spÃ©cialisÃ©e', 'Actif', '2006-11-20');

-- --------------------------------------------------------

--
-- Structure de la table `upgrade`
--

DROP TABLE IF EXISTS `upgrade`;
CREATE TABLE IF NOT EXISTS `upgrade` (
  `ipUp` int(11) NOT NULL AUTO_INCREMENT,
  `idcompte` int(11) NOT NULL,
  `dateemission` datetime DEFAULT NULL,
  `dateupgrade` datetime DEFAULT NULL,
  `debupgrade` int(11) DEFAULT NULL,
  PRIMARY KEY (`ipUp`),
  KEY `AK_IDUpgrade_PK` (`ipUp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `idUser` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(254) DEFAULT NULL,
  `prenom` varchar(254) DEFAULT NULL,
  `telephone` varchar(254) DEFAULT NULL,
  `adress` varchar(254) DEFAULT NULL,
  `email` varchar(254) DEFAULT NULL,
  `login` varchar(50) NOT NULL,
  `pass` varchar(50) NOT NULL,
  PRIMARY KEY (`idUser`),
  KEY `AK_Identifier_1` (`idUser`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`idUser`, `nom`, `prenom`, `telephone`, `adress`, `email`, `login`, `pass`) VALUES
(1, 'Mr Badis', 'Badis', '71223665', 'Mannouba CCK', 'badis@cck.rnu.tn', 'badis', 'e10adc3949ba59abbe56e057f20f883e');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `equipment`
--
ALTER TABLE `equipment`
  ADD CONSTRAINT `FK_Association_2` FOREIGN KEY (`idetab`) REFERENCES `etablissement` (`idetab`);

--
-- Contraintes pour la table `etablissement`
--
ALTER TABLE `etablissement`
  ADD CONSTRAINT `FK_Association_3` FOREIGN KEY (`idUser`) REFERENCES `user` (`idUser`);

--
-- Contraintes pour la table `lignes`
--
ALTER TABLE `lignes`
  ADD CONSTRAINT `FK_Association_1` FOREIGN KEY (`idetab`) REFERENCES `etablissement` (`idetab`),
  ADD CONSTRAINT `FK_Association_6` FOREIGN KEY (`idfornisseur`) REFERENCES `forniseur` (`idfornisseur`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
