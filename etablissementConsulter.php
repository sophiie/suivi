<!DOCTYPE html>
<html lang="fr">

  <head>

    <meta charset="utf-16">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">

    <title>Ramayana - Free Bootstrap 4 CSS Template</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="assets/css/fontawesome.css">
    <link rel="stylesheet" href="assets/css/templatemo-style.css">
    <link rel="stylesheet" href="assets/css/owl.css">

  </head>

<body class="is-preload">
    <!-- Wrapper -->
    <div id="wrapper">

      <!-- Main -->
        <div id="main">
          <div class="inner">


			<?php include("sidebar.php"); ?>



	<?php include("config/connect.php"); 

    $idetab=$_GET['idetab'];

	$sql = "SELECT * FROM etablissement where idetab = $idetab";
	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
	  // output data of each row
	  if($row = $result->fetch_assoc()) {
		  ?>



 <div class="alternate-table">
                      <table>

                        <tbody>
							<tr>
								<td>ID Etablissement</td>
								<td><?php echo $row['idetab'];?></td>
							</tr>
							<tr>
								<td>Etablissement</td>
								<td><?php echo $row['nom'];?></td>
							</tr>
							<tr>
								<td>Fournisseur</td>
								<td><?php echo $row['fo'];?></td>
							</tr>
							<tr>
								<td>Debit</td>
								<td><?php echo $row['debit'];?></td>
							</tr>
							<tr>
								<td>Debit upgrade</td>
								<td><?php echo $row['debupgrade'];?></td>
							</tr>
							<tr>
								<td>Date création</td>
								<td><?php echo $row['datecreation'];?></td>
							</tr>
							<tr>
								<td>Date upgrade</td>
								<td><?php echo $row['dateupgrade'];?></td>
							</tr>
							<tr>
								<td>IP LAN</td>
								<td><?php echo $row['iplan'];?></td>
							</tr>
							<tr>
								<td>IP WAN</td>
								<td><?php echo $row['ipwan'];?></td>
							</tr>
							<tr>
								<td>Adresse</td>
								<td><?php echo $row['adresse'];?></td>
							</tr>
							<tr>
								<td>Tél</td>
								<td><?php echo $row['tel'];?></td>
							</tr>
							<tr>
								<td>Responsable</td>
								<td><?php echo $row['responsable'];?></td>
							</tr>
  
                        </tbody>
                      </table>
                    </div>

					<div class="border-rounded-button">
                          <a href="etablissementEditer.php?idetab=<?php echo $idetab;?>">Modifier les informations</a>
                        </div>
                        </div>                  


<?php  }
	} else {
	  echo "Pas des établissements";
	}
	?>










   
          </div>
	    </div>
	</div>

  <!-- Scripts -->
  <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/browser.min.js"></script>
    <script src="assets/js/breakpoints.min.js"></script>
    <script src="assets/js/transition.js"></script>
    <script src="assets/js/owl-carousel.js"></script>
    <script src="assets/js/custom.js"></script>
</body>


  </body>
</html>