<!DOCTYPE html>
<html lang="fr">

  <head>

    <meta charset="utf-16">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">

    <title>Gestion Equipements</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="assets/css/fontawesome.css">
    <link rel="stylesheet" href="assets/css/templatemo-style.css">
    <link rel="stylesheet" href="assets/css/owl.css">

  </head>

<body class="is-preload">
    <!-- Wrapper -->
    <div id="wrapper">

      <!-- Main -->
        <div id="main">
          <div class="inner">


			<?php include("sidebar.php");
				if (isset($_GET['message']) && $_GET['message'] == '1'){
					echo "<div>Etablissement ajouté avec succès</div>";
				}
				if (isset($_GET['message']) && $_GET['message'] == '2'){
					echo "<div>Etablissement supprimé avec succès</div>";
				}
			?>

<br>
                      <div class="col-md-6">
						<div class="border-rounded-button"><a href="equipementAdd.php">Ajout Equipement</a></div>
                        </div>



 <div class="alternate-table">
                      <table>
                        <thead>
                          <tr>
                            <th>Equipement</th>
                            <th>Etablissement</th>
                            <th>Designation</th>
                            <th>Model</th>
                            <th>Etat Création</th>
                            <th width="25%">Actions</th>
                          </tr>
                        </thead>
                        <tbody>
		<?php include("config/connect.php"); 

	$sql = "SELECT * FROM equipment";
	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
	  // output data of each row
	  while($row = $result->fetch_assoc()) {		 
                # Réquipérer nom de l'établissments
				$sqlEtab = "SELECT * FROM etablissement where idetab=".$row['idetab'];
			  $resultEtab = $conn->query($sqlEtab);
			  $rowEtab = $resultEtab->fetch_assoc();

       

		  echo "<tr>
      <td>".$row['idequip']."</td>
      <td>".$rowEtab['nom']."</td>
					<td>".$row['DesignationEquip']."</td>
					<td>".$row['model']."</td>
					<td>".$row['etatCreation']."</td>
					<td>	
						<a href='equipementConsulter.php?idequip=".$row['idequip']."'>Consulter</a>
						<a href='equipementEditer.php?idequip=".$row['idequip']."'>Editer</a>
						<a href='equipementSupprimer.php?idequip=".$row['idequip']."'>Supprimer</a>
					</td>  
				</tr>";
	  }
	} else {
	  echo "Pas des Equipements";
	}


?>				
						  
                        </tbody>
                      </table>
                      </ul>
                    </div>
          </div>
	    </div>
	</div>

  <!-- Scripts -->
  <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/browser.min.js"></script>
    <script src="assets/js/breakpoints.min.js"></script>
    <script src="assets/js/transition.js"></script>
    <script src="assets/js/owl-carousel.js"></script>
    <script src="assets/js/custom.js"></script>
</body>


  </body>
</html>