<!DOCTYPE html>
<html lang="fr">

  <head>

    <meta charset="utf-16">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">

    <title>Ramayana - Free Bootstrap 4 CSS Template</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="assets/css/fontawesome.css">
    <link rel="stylesheet" href="assets/css/templatemo-style.css">
    <link rel="stylesheet" href="assets/css/owl.css">

  </head>

<body class="is-preload">
    <!-- Wrapper -->
    <div id="wrapper">

      <!-- Main -->
        <div id="main">
          <div class="inner">


			<?php include("sidebar.php"); ?>



	<?php include("config/connect.php"); 

    $idligne=$_GET['idligne'];

    $sql = "SELECT * FROM `lignes` where `idligne` = $idligne ";
    $result = $conn->query($sql);

	if ($result->num_rows > 0) {
	  // output data of each row
	  if($row = $result->fetch_assoc()) {
		  ?>



 <div class="alternate-table">
                      <table>

                        <tbody>
							<tr>
								<td width="50%">Etablissement</td>
								<td>                
                <?php
                $sqlEtab = "SELECT * FROM etablissement where idetab=".$row['idetab'];
                $resultEtab = $conn->query($sqlEtab);
                $rowEtab = $resultEtab->fetch_assoc();
								 echo $rowEtab['nom'];?>                
                </td>
							</tr>
							<tr>
								<td>Fournissuer</td>
								<td><?php
                $sqlEtab = "SELECT * FROM forniseur where idfornisseur=".$row['idfornisseur'];
                $resultEtab = $conn->query($sqlEtab);
                $rowEtab = $resultEtab->fetch_assoc();
								 echo $rowEtab['designation'];?>  </td>
							</tr>
							<tr>
								<td>Adresse</td>
								<td><?php echo $row['adress'];?></td>
							</tr>

							<tr>
								<td>Débit ligne</td>
								<td><?php echo $row['debitligne'];?></td>
							</tr>

							<tr>
								<td>Téchnologie</td>
								<td><?php echo $row['technologie'];?></td>
							</tr>

							<tr>
								<td>Etat</td>
								<td><?php echo $row['etat'];?></td>
							</tr>

							<tr>
								<td>Date emision</td>
								<td><?php echo $row['datemission'];?></td>
							</tr>
              
							<tr>
								<td>Date Cloture</td>
								<td><?php echo $row['datecloture'];?></td>
							</tr>



                        </tbody>
                      </table>
                    </div>

               <table><tr>
               <td width="50%"><div class="border-rounded-button">
                    <a href="lignesMain.php">Retour au gestion des lignes</a>
                     </div>
                     </td>
               <td><div class="border-rounded-button">
               <a href="lignesEditer.php?idligne=<?php echo $idligne;?>">Modifier les informations</a>
                     </div></td>
               </tr> 
               </table>

            </div>                  


<?php  }
	} else {
	  echo "Pas des lignes";
	}
	?>










   
          </div>
	    </div>
	</div>

  <!-- Scripts -->
  <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/browser.min.js"></script>
    <script src="assets/js/breakpoints.min.js"></script>
    <script src="assets/js/transition.js"></script>
    <script src="assets/js/owl-carousel.js"></script>
    <script src="assets/js/custom.js"></script>
</body>


  </body>
</html>